<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Video;
use App\VideoFrame;

class ResultTest extends TestCase
{
    // use RefreshDatabase;
    /**
     * Test videos and their frames extracted into database.
     *
     * @return void
     */
    public function test_videos_and_their_frames_extracted_into_database() {
        for($u = 1; $u <= 3; $u++) {
            // Create user.
            $user = factory(User::class)->create();
            dump("User «{$user->name}» with the id of #{$user->id} created.");

            // Create video.
            $videoCount = mt_rand(1, 3);
            for($vC = 1; $vC <= $videoCount; $vC++) {
                $videoDuration = mt_rand(3, 10); // in seconds
                $video = factory(Video::class)->create([
                    'user_id' => $user->id,
                    'duration' => $videoDuration,
                ]);
                dump("Video with the name of «{$video->title}» and the id of #{$video->id} created.");
                $user->uploadVideo($video);
                dump("Video #{$video->id} related to user #{$video->user_id}.");

                // Create video frames
                $framesPerSecond = 1; // It was 30.
                $videoFramesCount = $videoDuration * $framesPerSecond;
                $frameNumber = 1;
                for($vF = 1; $vF <= $videoFramesCount; $vF++) {
                    $frameResult = mt_rand(10, 100);
                    $videoFrame = factory(VideoFrame::class)->create([
                        'video_id' => $video->id,
                        'frame_number' => $frameNumber++,
                        'result' => $frameResult,
                    ]);
                    dump("Video frame of the video «{$video->title}» with the frame number #{$videoFrame->frame_number} created.");
                    $video->extractFrames($videoFrame);
                    dump("Video frame #{$videoFrame->frame_number} related to video #{$videoFrame->video_id}.");
                }
            }
        }

        // It is hard coded true, because any of the insertions above throws error, it will not reach this line.
        $this->assertTrue(true);
    }

    /**
     * Test the relations of the users, videos and video frames.
     * To test this method, we need to have some data. In order to have some data,
     * we should comment the `use RefreshDatabase` trait at the top
     * and run full test (Filtering this method without having data is useless).
     * @return void
     */
    public function test_the_relations_of_the_users_videos_and_video_frames() {
        for($u = 0; $u <= 3; $u++) {
            $user = User::skip($u)->first();
            if($user) {
                $userVideos = Video::where('user_id', $user->id)->get();
                $this->assertEquals($userVideos, $user->videos);
                dump("The relation between the user #{$user->id} and his videos working => User has many videos.");

                foreach($userVideos as $userVideo) {
                    $userVideoFrames = VideoFrame::where('video_id', $userVideo->id)->get();
                    $this->assertEquals($userVideoFrames, $userVideo->frames);
                    dump("The relation between the video #{$userVideo->id} and its frames working => Video has many frames.");

                    $resultAvgWithRelation = $userVideo->framesResultAverage[0]->result_avg;
                    // Changing the round precision may change the result of average correctness.
                    $resultAvgWithoutRelation = round($userVideoFrames->avg('result'), 2);
                    $correctness = $resultAvgWithoutRelation == $resultAvgWithRelation ? 'Yes' : "No: $resultAvgWithoutRelation != $resultAvgWithRelation";
                    dump("The frames average of the video «{$userVideo->title}» calculated correctly? $correctness");
                    $this->assertEquals($resultAvgWithoutRelation, $resultAvgWithRelation);
                }
            } else {
                $this->assertTrue(true);
                dump("No more user exists...!");
                break;
            }
        }
    }
}
