<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('result_video', 'VideoController@index')->name('result-video');
Route::get('result_user', 'UserController@index')->name('result-user-all');
Route::get('result_user/{userId}', 'UserController@show')->name('result-user-one');
Route::get('result', function() {
	set_time_limit(999);
	$root = __DIR__.'/../';
	$test = shell_exec('"'.$root.'vendor/bin/phpunit.bat" --configuration '.$root.'phpunit.xml');
	dd($test);
});
