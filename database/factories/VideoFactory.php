<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Video;
use Faker\Generator as Faker;

$factory->define(Video::class, function (Faker $faker, $user) {
    return [
        'title' => $faker->word,
        'path' => 'test.mp4',
    ];
});