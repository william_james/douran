<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\VideoFrame;
use Faker\Generator as Faker;

$factory->define(VideoFrame::class, function (Faker $faker) {
    return [
    	'name' => $faker->word,
    ];
});
