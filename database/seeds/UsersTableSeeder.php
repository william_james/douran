<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'id' => 1,
            'name' => 'William James',
            'email' => 'w@j.com',
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => \Hash::make('123'),
            'remember_token' => NULL,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 2,
            'name' => 'Marie Curie',
            'email' => 'v@s.com',
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => \Hash::make('123'),
            'remember_token' => NULL,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]]);
    }
}