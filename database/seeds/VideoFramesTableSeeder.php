<?php

use Illuminate\Database\Seeder;

class VideoFramesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('video_frames')->insert([[
            'id' => 1,
            'video_id' => 1,
            'frame_number' => 1,
            'name' => 'test',
            'result' => 50,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 2,
            'video_id' => 1,
            'frame_number' => 2,
            'name' => 'test',
            'result' => 51,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 3,
            'video_id' => 1,
            'frame_number' => 3,
            'name' => 'test',
            'result' => 52,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 4,
            'video_id' => 1,
            'frame_number' => 4,
            'name' => 'test',
            'result' => 53,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 5,
            'video_id' => 1,
            'frame_number' => 5,
            'name' => 'test',
            'result' => 54,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 6,
            'video_id' => 1,
            'frame_number' => 6,
            'name' => 'test',
            'result' => 53,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 7,
            'video_id' => 1,
            'frame_number' => 7,
            'name' => 'test',
            'result' => 52,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 8,
            'video_id' => 1,
            'frame_number' => 8,
            'name' => 'test',
            'result' => 49,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 9,
            'video_id' => 1,
            'frame_number' => 9,
            'name' => 'test',
            'result' => 48,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 10,
            'video_id' => 1,
            'frame_number' => 10,
            'name' => 'test',
            'result' => 47,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 11,
            'video_id' => 2,
            'frame_number' => 1,
            'name' => 'test',
            'result' => 100,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 12,
            'video_id' => 2,
            'frame_number' => 2,
            'name' => 'test',
            'result' => 99,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 13,
            'video_id' => 2,
            'frame_number' => 3,
            'name' => 'test',
            'result' => 90,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 14,
            'video_id' => 2,
            'frame_number' => 4,
            'name' => 'test',
            'result' => 72,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 15,
            'video_id' => 2,
            'frame_number' => 5,
            'name' => 'test',
            'result' => 68,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]]);
    }
}