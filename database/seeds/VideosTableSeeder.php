<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('videos')->insert([[
            'id' => 1,
            'user_id' => 1,
            'title' => 'title1',
            'path' => 'somewhere/1.mp4',
            'duration' => 10,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ], [
            'id' => 2,
            'user_id' => 1,
            'title' => 'title2',
            'path' => 'somewhere/2.mp4',
            'duration' => 5,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]]);
    }
}