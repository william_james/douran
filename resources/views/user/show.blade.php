<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
	<meta charset="UTF-8">
	<title>Douran</title>
	<link rel="stylesheet" href="{!!asset('css/bootstrap.css')!!}">
	<link rel="stylesheet" href="{!!asset('css/app.css')!!}">
	<script src="{!!asset('app.js')!!}"></script>
</head>
<body>

	<div class="container">
		<div class="table-responsive">
			<table class="table table-primary text-center">
				<thead>
					<tr>
						<th>عنوان</th>
						<th>امتیاز</th>
					</tr>
				</thead>
				<tbody>
					@foreach($userVideos as $video)
						<tr>
							<td>{{$video->title}}</td>
							<td>{!!$video->framesResultAverage[0]->result_avg!!}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</body>
</html>