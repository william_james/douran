<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
	<meta charset="UTF-8">
	<title>Douran</title>
	<link rel="stylesheet" href="{!!asset('css/bootstrap.css')!!}">
	<link rel="stylesheet" href="{!!asset('css/app.css')!!}">
	<script src="{!!asset('js/app.js')!!}"></script>
</head>
<body>

	<div class="container">
		<div class="table-responsive">
			<table class="table table-primary text-center">
				<thead>
					<tr>
						<th>نام کاربر</th>
						<th>عنوان و امتیاز</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
							<td>{{$user->name}}</td>
							<td>
								<div class="video-list-wrapper">
									@forelse($user->videos as $video)
										<ul class="list-group list-group-horizontal justify-content-center">
											<li class="list-group-item">عنوان: {{$video->title}}</li>
											<li class="list-group-item">امتیاز: {!!$video->framesResultAverage[0]->result_avg!!}</li>
										</ul>
										<br>
									@empty
										<strong>بدون ویدیو</strong>
									@endforelse
									<strong>
										{{-- This can be iterated in the controller
											but the performance would be a little slow --}}
										{!!$user->videos->map(function($video) {
											return $video['framesResultAverage'][0]['result_avg'];
										})->sum()!!}
									</strong>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</body>
</html>