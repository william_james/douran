<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Video;

class UserController extends Controller
{
	public function index() {
		/**
		* Users videos and the average of their frames result.
		*/
		$users = User::with('videos.framesResultAverage')->get(); // parginate()

		return view('user.index', compact('users'));
	}

	public function show($userId) {
		/**
		* One user videos and the average of their frames result.
		*/
		$userVideos = Video::with('framesResultAverage')->owner($userId)->get(); // parginate()

		return view('user.show', compact('userVideos'));
	}
}
