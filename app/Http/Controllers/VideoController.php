<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
	public function index() {
		$videos = Video::with('framesResultAverage')->get(); // parginate()

		return view('video.index', compact('videos'));
	}
}
