<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoFrame extends Model
{
	protected $fillable = [];

	public function video() {
        return $this->belongsTo('App\Video');
    }
}
