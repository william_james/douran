<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
	protected $fillable = [];

	public function owner() {
        return $this->belongsTo('App\User');
    }

    public function frames() {
    	return $this->hasMany('App\VideoFrame');
    }

    public function framesResultAverage() {
        return $this->frames()
                    ->selectRaw('video_id, ROUND(AVG(video_frames.result), 2) AS result_avg')
                    ->groupBy('video_id');
    }

    public function extractFrames($videoFrame) {
        $this->frames()->save($videoFrame);
    }

    public function scopeOwner($query, $userId = null) {
        if(is_null($userId)) {
            $userId = auth()->user()->id;
        }

        return $query->where('user_id', $userId);
    }
}
